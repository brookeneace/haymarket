<?php
/*
Plugin Name: DeGutenizer
Plugin URI: https://wordpress.org/plugins/degutenizer/
Description: De-gutenize your site (disable the Gutenberg editor)
Author: pipdig
Author URI: https://www.pipdig.co
Version: 1.1
License: GPLv2 or later
*/

// de nada