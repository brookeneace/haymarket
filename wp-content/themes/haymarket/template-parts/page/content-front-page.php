<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<article id="post-<?php the_ID(); ?>"  >



		<div class="wrap">
			
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>

				<?php twentyseventeen_edit_link( get_the_ID() ); ?>

		

			<div class="entry-content">
				<?php
					/* translators: %s: Name of current post */
					the_content();
					?>
			</div><!-- .entry-content -->

		</div><!-- .wrap -->


</article><!-- #post-## -->
